### Установка зависимостей:

Poetry
```shell
poetry install
```

### Проверки

Проверка линтером
```shell
black src
```

Проверка тестов
```shell
python -m doctest --option ELLIPSIS src/models.py
```

Проверка типизации
```shell
mypy --strict --show-error-codes src
```