from __future__ import annotations

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import roc_auc_score

import numpy as np
import datetime
from typing import Optional, Iterable, Any


class Client:
    def __init__(
        self,
        seniority: int,
        home: int,
        time: int,
        age: int,
        marital: int,
        records: int,
        job: int,
        expenses: int,
        income: int,
        assets: int,
        debt: int,
        amount: int,
        price: int,
        status: Optional[int] = None,
    ) -> None:
        self.seniority = seniority
        self.home = home
        self.age = age
        self.time = time
        self.marital = marital
        self.records = records
        self.job = job
        self.expenses = expenses
        self.income = income
        self.assets = assets
        self.debt = debt
        self.amount = amount
        self.price = price
        self.status = status
        self.classification: Optional[int] = None

    def __repr__(self) -> str:
        if self.status is None:
            known_unknown = "UnknownClient"
        else:
            known_unknown = "KnownClient"
        if self.classification is None:
            classification = ""
        else:
            classification = f", classification={self.classification!r}"
        return (
            f"{known_unknown}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}, "
            f"status={self.status!r}"
            f"{classification}"
            f")"
        )

    def classify(self, classification: int) -> None:
        self.classification = classification

    def matches(self) -> bool:
        return self.status == self.classification

    def get_dict_attributes(self) -> dict[str, Any]:
        dict_attributes = self.__dict__.copy()
        dict_attributes.pop("status", None)
        dict_attributes.pop("classification", None)
        return dict_attributes


class Model:
    def __init__(
        self, max_depth: int, min_samples_leaf: int, training: "TrainingData"
    ) -> None:
        self.max_depth = max_depth
        self.min_samples_leaf = min_samples_leaf
        self.data: TrainingData = training
        self.quality: float

    def test(self) -> None:
        """
        Проверка на тестовом наборе данных
        """
        training_data = self.data
        test_data = training_data.testing
        y_pred = self.classify(test_data)
        y_true = [test_client.status for test_client in test_data]
        self.quality = roc_auc_score(y_true, y_pred)
        for i in range(len(y_pred)):
            test_data[i].classification = y_pred[i]

    def classify(self, clients: list[Client]) -> Any:
        training_data = self.data
        x_train = [
            list(train_client.get_dict_attributes().values())
            for train_client in training_data.training
        ]
        y_train = [train_client.status for train_client in training_data.training]
        x_pred = [list(client.get_dict_attributes().values()) for client in clients]

        classifier = DecisionTreeClassifier(
            max_depth=self.max_depth, min_samples_leaf=self.min_samples_leaf
        )
        classifier = classifier.fit(x_train, y_train)
        y_pred = classifier.predict(x_pred).tolist()
        return y_pred


class TrainingData:
    """Тренировочные данные"""

    def __init__(self, name: str) -> None:
        """

        Args:
            name (str): наименование набора
        """
        self.name = name
        self.uploaded: datetime.datetime
        self.tested: datetime.datetime
        self.training: list[Client] = []
        self.testing: list[Client] = []
        self.tuning: list[Model] = []

    def load(self, raw_data_soruce: Iterable[dict[str, str]]) -> None:
        """
        Загружает и разбивается исходные данные

        Args:
            raw_data_soruce (Iterable[dict[str, str]]): источник сырых данных
        """

        for n, row in enumerate(raw_data_soruce):
            client = Client(
                seniority=int(row["seniority"]),
                home=int(row["home"]),
                time=int(row["age"]),
                age=int(row["age"]),
                marital=int(row["marital"]),
                records=int(row["records"]),
                job=int(row["job"]),
                expenses=int(row["expenses"]),
                income=int(row["income"]),
                assets=int(row["assets"]),
                debt=int(row["debt"]),
                amount=int(row["amount"]),
                price=int(row["price"]),
                status=int(row["status"]),
            )

            if n % 5 == 0:
                self.testing.append(client)
            else:
                self.training.append(client)

        self.uploaded = datetime.datetime.now(tz=datetime.timezone.utc)

    def test(self, parameter: Model) -> None:
        """Тестирование

        Args:
            parameter (Model): Модель
        """

        parameter.test()
        self.tuning.append(parameter)
        self.tested = datetime.datetime.now(tz=datetime.timezone.utc)

    def classify(self, parameter: Model, clients: list[Client]) -> list[Client]:
        """Классифицировать очередного клиента

        Args:
            parameter (Model): Модель дерева решений
            clients (Client): Клиент

        Returns:
            Clients: Классифицированные клиенты
        """
        classifications = parameter.classify(clients)
        for i, classification in enumerate(classifications):
            clients[i].classify(classification)
        return clients


test_Client = """
>>> c_1 = Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = 1)
>>> c_1
KnownClient(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846, status=1)
"""

test_TestingKnownSample = """
>>> c_3 = Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = 1)
>>> c_3.classification = 1
>>> c_3
KnownClient(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846, status=1, classification=1)
"""

test_TrainingKnownClient = """
>>> c_4 = Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = 1)
>>> c_4
KnownClient(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846, status=1)
"""

test_UnknownClient = """
>>> c_2 = Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = None)
>>> c_2
UnknownClient(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846, status=None)
"""

test_ClassifiedClient = """
>>> c_5 = Client(seniority = 17, home = 1, age = 58, time = 60, marital = 3, records = 1, job = 1, expenses = 48, income = 131, assets = 0, debt = 0, amount = 1000, price = 1685)
>>> c_5.classify(1)
>>> c_5
UnknownClient(seniority=17, home=1, age=58, time=60, marital=3, records=1, job=1, expenses=48, income=131, assets=0, debt=0, amount=1000, price=1685, status=None, classification=1)
"""

test_Model = """
>>> td = TrainingData('test')
>>> c_2 = Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = 1)
>>> c_3 = Client(seniority = 10, home = 2, age = 46, time = 36, marital = 2, records = 2, job = 3, expenses = 90, income = 200, assets = 3000, debt = 0, amount = 2000, price = 2985, status = 2)
>>> td.testing = [c_2, c_3]
>>> td.training = [c_2, c_3]
>>> mod = Model(max_depth = 2, min_samples_leaf = 1, training = td)
>>> u = [Client(seniority = 17, home = 1, age = 58, time = 60, marital = 3, records = 1, job = 1, expenses = 48, income = 131, assets = 0, debt = 0, amount = 1000, price = 1685)]
>>> td.classify(mod, u)
[UnknownClient(seniority=17, home=1, age=58, time=60, marital=3, records=1, job=1, expenses=48, income=131, assets=0, debt=0, amount=1000, price=1685, status=None, classification=2)]
>>> mod.test()
>>> print(f"data={td.name!r}, max_depth={mod.max_depth}, quality={mod.quality}")
data='test', max_depth=2, quality=1.0
"""

test_TrainingData = """
>>> td = TrainingData('test')
>>> raw_data = [
... {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 1},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 2},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 1},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 2},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 1},
... {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 2},
... {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 1},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 2},
... ]
>>> td.load(raw_data)
>>> mod = Model(max_depth = 2, min_samples_leaf = 1, training = td)
>>> len(td.training)
6
>>> len(td.testing)
2
>>> td.test(mod)
>>> print(f"data={td.name!r}, max_depth={mod.max_depth}, quality={mod.quality}")
data='test', max_depth=2, quality=0.5
"""
__test__ = {name: case for name, case in globals().items() if name.startswith("test_")}
