### Установка зависимостей:

Pip
```shell
pip install -r reqirements.txt
```
Poetry
```shell
poetry install
```

### Проверки

Проверка линтером
```shell
black stage_3
```

Проверка тестов
```shell
python -m doctest --option ELLIPSIS src/models.py
```

Проверка типизации
```shell
mypy --strict --show-error-codes src
```