from __future__ import annotations

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import roc_auc_score

import datetime
from typing import Iterable, Any, Sequence, Optional

import weakref


class Client:
    def __init__(
        self,
        seniority: int,
        home: int,
        time: int,
        age: int,
        marital: int,
        records: int,
        job: int,
        expenses: int,
        income: int,
        assets: int,
        debt: int,
        amount: int,
        price: int,
    ) -> None:
        self.seniority = seniority
        self.home = home
        self.age = age
        self.time = time
        self.marital = marital
        self.records = records
        self.job = job
        self.expenses = expenses
        self.income = income
        self.assets = assets
        self.debt = debt
        self.amount = amount
        self.price = price

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}"
            f")"
        )

    def get_dict_attributes(self) -> dict[str, Any]:
        dict_attributes = self.__dict__.copy()
        dict_attributes.pop("status", None)
        dict_attributes.pop("classification", None)
        return dict_attributes


class KnownClient(Client):
    def __init__(
        self,
        seniority: int,
        home: int,
        time: int,
        age: int,
        marital: int,
        records: int,
        job: int,
        expenses: int,
        income: int,
        assets: int,
        debt: int,
        amount: int,
        price: int,
        status: int | None = None,
    ) -> None:
        super().__init__(
            seniority,
            home,
            time,
            age,
            marital,
            records,
            job,
            expenses,
            income,
            assets,
            debt,
            amount,
            price,
        )
        self.status = status

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}, "
            f"status={self.status!r}"
            f")"
        )


class TrainingKnownClient(KnownClient):
    pass


class TestingKnownClient(KnownClient):
    def __init__(
        self,
        seniority: int,
        home: int,
        time: int,
        age: int,
        marital: int,
        records: int,
        job: int,
        expenses: int,
        income: int,
        assets: int,
        debt: int,
        amount: int,
        price: int,
        status: int,
        classification: int | None = None,
    ) -> None:
        super().__init__(
            seniority,
            home,
            time,
            age,
            marital,
            records,
            job,
            expenses,
            income,
            assets,
            debt,
            amount,
            price,
            status,
        )
        self.classification = classification

    def matches(self) -> bool:
        return self.status == self.classification

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}, "
            f"status={self.status!r}, "
            f"classification={self.classification!r}"
            f")"
        )


class UnknownClient(Client):
    pass


class ClassifiedClient(Client):
    def __init__(self, classification: int | None, client: UnknownClient) -> None:
        super().__init__(
            client.seniority,
            client.home,
            client.time,
            client.age,
            client.marital,
            client.records,
            client.job,
            client.expenses,
            client.income,
            client.assets,
            client.debt,
            client.amount,
            client.price,
        )
        self.classification = classification

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}, "
            f"classification={self.classification!r}"
            f")"
        )


class Model:
    def __init__(
        self, max_depth: int, min_samples_leaf: int, training: "TrainingData"
    ) -> None:
        self.max_depth = max_depth
        self.min_samples_leaf = min_samples_leaf
        self.data: weakref.ReferenceType["TrainingData"] = weakref.ref(training)
        self.quality: float

    def test(self) -> None:
        """
        Проверка на тестовом наборе данных
        """
        training_data: "TrainingData" | None = self.data()
        if not training_data:
            raise RuntimeError("Broken Weak Reference")
        test_data = training_data.testing
        y_pred = self.classify(test_data)
        y_true = [test_client.status for test_client in test_data]
        self.quality = roc_auc_score(y_true, y_pred)
        for i in range(len(y_pred)):
            test_data[i].classification = y_pred[i]

    def classify(self, clients: Sequence[UnknownClient | TestingKnownClient]) -> Any:
        training_data = self.data()
        if not training_data:
            raise RuntimeError("No trainig object")
        x_train = [
            list(train_client.get_dict_attributes().values())
            for train_client in training_data.training
        ]
        y_train = [train_client.status for train_client in training_data.training]
        x_pred = [list(client.get_dict_attributes().values()) for client in clients]

        classifier = DecisionTreeClassifier(
            max_depth=self.max_depth, min_samples_leaf=self.min_samples_leaf
        )
        classifier = classifier.fit(x_train, y_train)
        y_pred = classifier.predict(x_pred).tolist()
        return y_pred


class TrainingData:
    """Тренировочные данные"""

    def __init__(self, name: str) -> None:
        """

        Args:
            name (str): наименование набора
        """
        self.name = name
        self.uploaded: datetime.datetime
        self.tested: datetime.datetime
        self.training: list[TrainingKnownClient] = []
        self.testing: list[TestingKnownClient] = []
        self.tuning: list[Model] = []

    def load(self, raw_data_soruce: list[dict[str, str]]) -> None:
        """
        Загружает и разбивается исходные данные

        Args:
            raw_data_soruce (Iterable[dict[str, str]]): источник сырых данных
        """

        for n, row in enumerate(raw_data_soruce):
            if n % 5 == 0:
                client_test = TestingKnownClient(
                    seniority=int(row["seniority"]),
                    home=int(row["home"]),
                    time=int(row["age"]),
                    age=int(row["age"]),
                    marital=int(row["marital"]),
                    records=int(row["records"]),
                    job=int(row["job"]),
                    expenses=int(row["expenses"]),
                    income=int(row["income"]),
                    assets=int(row["assets"]),
                    debt=int(row["debt"]),
                    amount=int(row["amount"]),
                    price=int(row["price"]),
                    status=int(row["status"]),
                )
                self.testing.append(client_test)
            else:
                client_train = TrainingKnownClient(
                    seniority=int(row["seniority"]),
                    home=int(row["home"]),
                    time=int(row["age"]),
                    age=int(row["age"]),
                    marital=int(row["marital"]),
                    records=int(row["records"]),
                    job=int(row["job"]),
                    expenses=int(row["expenses"]),
                    income=int(row["income"]),
                    assets=int(row["assets"]),
                    debt=int(row["debt"]),
                    amount=int(row["amount"]),
                    price=int(row["price"]),
                    status=int(row["status"]),
                )
                self.training.append(client_train)

        self.uploaded = datetime.datetime.now(tz=datetime.timezone.utc)

    def test(self, parameter: Model) -> None:
        """Тестирование

        Args:
            parameter (Model): Модель
        """

        parameter.test()
        self.tuning.append(parameter)
        self.tested = datetime.datetime.now(tz=datetime.timezone.utc)

    def classify(
        self, parameter: Model, clients: list[UnknownClient]
    ) -> list[ClassifiedClient]:
        """Классифицировать очередного клиента

        Args:
            parameter (Model): Модель дерева решений
            clients (Client): Клиент

        Returns:
            Clients: Классифицированные клиенты
        """
        classifications = parameter.classify(clients)
        classified_client: list[ClassifiedClient] = []
        for i, classification in enumerate(classifications):
            classified_client.append(ClassifiedClient(classification, clients[i]))
        return classified_client


test_Client = """
>>> c_1 = Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846)
>>> c_1
Client(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846)
"""

test_TrainingKnownClient = """
>>> c_4 = TrainingKnownClient(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = 1)
>>> c_4
TrainingKnownClient(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846, status=1)
"""

test_TestingKnownSample = """
>>> c_3 = TestingKnownClient(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = 1)
>>> c_3.classification = 1
>>> c_3
TestingKnownClient(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846, status=1, classification=1)
"""

test_UnknownClient = """
>>> c_2 = UnknownClient(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846)
>>> c_2
UnknownClient(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846)
"""

test_ClassifiedClient = """
>>> c_5 = UnknownClient(seniority = 17, home = 1, age = 58, time = 60, marital = 3, records = 1, job = 1, expenses = 48, income = 131, assets = 0, debt = 0, amount = 1000, price = 1685)
>>> classified = ClassifiedClient(1, c_5)
>>> classified
ClassifiedClient(seniority=17, home=1, age=58, time=60, marital=3, records=1, job=1, expenses=48, income=131, assets=0, debt=0, amount=1000, price=1685, classification=1)
"""

test_Model = """
>>> td = TrainingData('test')
>>> c_2 = TestingKnownClient(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846, status = 1)
>>> c_3 = TestingKnownClient(seniority = 10, home = 2, age = 46, time = 36, marital = 2, records = 2, job = 3, expenses = 90, income = 200, assets = 3000, debt = 0, amount = 2000, price = 2985, status = 2)
>>> train_c_1 = TrainingKnownClient(seniority = 10, home = 2, age = 46, time = 36, marital = 2, records = 2, job = 3, expenses = 90, income = 200, assets = 3000, debt = 0, amount = 2000, price = 2985, status = 2)
>>> train_c_2 = TrainingKnownClient(seniority = 17, home = 1, age = 58, time = 60, marital = 3, records = 1, job = 1, expenses = 48, income = 131, assets = 0, debt = 0, amount = 1000, price = 1685, status = 1)
>>> td.testing = [c_2, c_3]
>>> td.training = [train_c_1, train_c_2]
>>> mod = Model(max_depth = 2, min_samples_leaf = 1, training = td)
>>> u = [UnknownClient(seniority = 17, home = 1, age = 58, time = 60, marital = 3, records = 1, job = 1, expenses = 48, income = 131, assets = 0, debt = 0, amount = 1000, price = 1685)]
>>> mod.classify(u)
[1]
>>> mod.test()
>>> print(f"data={td.name!r}, max_depth={mod.max_depth}, quality={mod.quality}")
data='test', max_depth=2, quality=1.0
"""

test_TrainingData = """
>>> td = TrainingData('test')
>>> raw_data = [
... {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 1},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 2},
... {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 1},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 2},
... {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 1},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 2},
... {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 1},
... {"seniority": 10, "home": 2, "age": 46, "time": 36, "marital": 2, "records": 2, "job": 3, "expenses": 90, "income": 200, "assets": 3000, "debt": 0, "amount": 2000, "price": 2985, "status": 2},
... ]
>>> td.load(raw_data)
>>> mod = Model(max_depth = 2, min_samples_leaf = 1, training = td)
>>> len(td.training)
6
>>> len(td.testing)
2
>>> td.test(mod)
>>> print(f"data={td.name!r}, max_depth={mod.max_depth}, quality={mod.quality}")
data='test', max_depth=2, quality=1.0
"""

__test__ = {name: case for name, case in globals().items() if name.startswith("test_")}
