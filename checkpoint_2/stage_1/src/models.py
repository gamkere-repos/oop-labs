from __future__ import annotations

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import roc_auc_score

import datetime, csv, enum
from typing import Iterable, Any, Sequence, Optional, Set, cast, Iterator
from pathlib import Path

import weakref

class Purpose(enum.IntEnum):
    Training = 1
    Testing = 2


class Domain(Set[int]):
    def validate(self, value: int) -> int | None:
        if value in self:
            return value
        raise ValueError(f"{value!r}")


class DomainKeys(Set[str]):
    def validate(self, keys: set[str]) -> None:
        if keys != self:
            raise ValueError(f"invalid fields")


class InvalidClientError(ValueError):
    """Исходные данные имеют недопустимое представление данных"""


class BadClientRow(ValueError):
    """Валидация плохой строки"""
    pass

class Client:
    def __init__(
        self,
        seniority: int,
        home: int,
        time: int,
        age: int,
        marital: int,
        records: int,
        job: int,
        expenses: int,
        income: int,
        assets: int,
        debt: int,
        amount: int,
        price: int,
    ) -> None:
        self.seniority = seniority
        self.home = home
        self.age = age
        self.time = time
        self.marital = marital
        self.records = records
        self.job = job
        self.expenses = expenses
        self.income = income
        self.assets = assets
        self.debt = debt
        self.amount = amount
        self.price = price

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}"
            f")"
        )

    def get_dict_attributes(self) -> dict[str, Any]:
        dict_attributes = self.__dict__.copy()
        dict_attributes.pop("status", None)
        dict_attributes.pop("classification", None)
        return dict_attributes


class KnownClient(Client):
    def __init__(
        self,
        seniority: int,
        home: int,
        time: int,
        age: int,
        marital: int,
        records: int,
        job: int,
        expenses: int,
        income: int,
        assets: int,
        debt: int,
        amount: int,
        price: int,
        purpose: int,
        status: int | None = None,
    ) -> None:
        purpose_enum = Purpose(purpose)
        if purpose_enum not in {Purpose.Training, Purpose.Testing}:
            raise ValueError(
                f"invalid purpose: {purpose!r}: {purpose_enum}"
            )

        super().__init__(
            seniority,
            home,
            time,
            age,
            marital,
            records,
            job,
            expenses,
            income,
            assets,
            debt,
            amount,
            price,
        )
        self.purpose = purpose_enum
        self.status = status
        self._classification: Optional[int] = None

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}, "
            f"purpose={self.purpose}, "
            f"status={self.status!r}"
            f")"
        )

    @classmethod
    def from_dict(cls, row: dict[str, str]) -> KnownClient:
        status = Domain([0, 1, 2])
        try:
            return cls(
                seniority=int(row["seniority"]),
                home=int(row["home"]),
                time=int(row["time"]),
                age=int(row["age"]),
                marital=int(row["marital"]),
                records=int(row["records"]),
                job=int(row["job"]),
                expenses=int(row["expenses"]),
                income=int(row["income"]),
                assets=int(row["assets"]),
                debt=int(row["debt"]),
                amount=int(row["amount"]),
                price=int(row["price"]),
                purpose=int(row["purpose"]),
                status=status.validate(int(row['status'])),
            )
        except ValueError:
            raise InvalidClientError(f"invalid status in {row!r}")

    @property
    def classification(self) -> int | None:
        if self.purpose == Purpose.Testing:
            return self._classification
        else:
            raise AttributeError(f"Training clients have no classification")

    @classification.setter
    def classification(self, value: int) -> None:
        if self.purpose == Purpose.Testing:
            self._classification = value
        else:
            raise AttributeError(f"Training clients cannot be classification")

    def matches(self) -> bool:
        return self.status == self.classification


class UnknownClient(Client):
    @classmethod
    def from_dict(cls, row: dict[str, str]) -> UnknownClient:
        validKeys = DomainKeys(
            {
                "seniority",
                "home",
                "age",
                "time",
                "marital",
                "records",
                "job",
                "expenses",
                "income",
                "assets",
                "debt",
                "amount",
                "price",
            }
        )
        try:
            validKeys.validate(set(row.keys()))
            return cls(
                seniority=int(row["seniority"]),
                home=int(row["home"]),
                time=int(row["time"]),
                age=int(row["age"]),
                marital=int(row["marital"]),
                records=int(row["records"]),
                job=int(row["job"]),
                expenses=int(row["expenses"]),
                income=int(row["income"]),
                assets=int(row["assets"]),
                debt=int(row["debt"]),
                amount=int(row["amount"]),
                price=int(row["price"]),
            )
        except ValueError:
            raise InvalidClientError(f"invalid keys in {row!r}")

    @property
    def classification(self) -> int | None:
        return self._classification

    @classification.setter
    def classification(self, value: int) -> None:
        self._classification = value


class Model:
    def __init__(
        self, max_depth: int, min_samples_leaf: int, training: "TrainingData"
    ) -> None:
        self.max_depth = max_depth
        self.min_samples_leaf = min_samples_leaf
        self.data: weakref.ReferenceType["TrainingData"] = weakref.ref(training)
        self.quality: float

    def test(self) -> None:
        """
        Проверка на тестовом наборе данных
        """
        training_data: "TrainingData" | None = self.data()
        if not training_data:
            raise RuntimeError("Broken Weak Reference")
        test_data = training_data.testing
        y_pred = self.classify(test_data)
        y_true = [test_client.status for test_client in test_data]
        self.quality = roc_auc_score(y_true, y_pred)
        for i in range(len(y_pred)):
            test_data[i].classification = y_pred[i]

    def classify(self, clients: Sequence[UnknownClient | KnownClient]) -> Any:
        training_data = self.data()
        if not training_data:
            raise RuntimeError("No trainig object")
        x_train = [
            list(train_client.get_dict_attributes().values())
            for train_client in training_data.training
        ]
        y_train = [train_client.status for train_client in training_data.training]
        x_pred = [list(client.get_dict_attributes().values()) for client in clients]

        classifier = DecisionTreeClassifier(
            max_depth=self.max_depth, min_samples_leaf=self.min_samples_leaf
        )
        classifier = classifier.fit(x_train, y_train)
        y_pred = classifier.predict(x_pred).tolist()
        return y_pred




class ClientReader:
    """_summary_

    Returns:
        _type_: _description_
    """
    target_class = KnownClient
    header = [
        "seniority",
        "home",
        "age",
        "time",
        "marital",
        "records",
        "job",
        "expenses",
        "income",
        "assets",
        "debt",
        "amount",
        "price",
        "purpose",
        "status"
    ]

    def __init__(self, source: Path) -> None:
        self.source = source

    def client_iter(self) -> Iterator[KnownClient]:
        target_class = self.target_class
        with self.source.open() as source_file:
            reader = csv.DictReader(source_file, self.header)
            for row in reader:
                try:
                    client = target_class.from_dict(row)
                except ValueError as exception:
                    raise BadClientRow(f"invalid {row!r}")
                yield client


class TrainingData:
    """Тренировочные данные"""

    def __init__(self, name: str) -> None:
        """

        Args:
            name (str): наименование набора
        """
        self.name = name
        self.uploaded: datetime.datetime
        self.tested: datetime.datetime
        self.training: list[KnownClient] = []
        self.testing: list[KnownClient] = []
        self.tuning: list[Model] = []

    def load(self, client_reader: ClientReader) -> None:
        """
        Загружает и разбивается исходные данные

        Args:
            raw_data_soruce (Iterable[dict[str, str]]): источник сырых данных
        """
        for client in client_reader.client_iter():
            if client.purpose == Purpose.Testing:
                self.testing.append(client)
            else:
                self.training.append(client)

        self.uploaded = datetime.datetime.now(tz=datetime.timezone.utc)

    def test(self, parameter: Model) -> None:
        """Тестирование

        Args:
            parameter (Model): Модель
        """

        parameter.test()
        self.tuning.append(parameter)
        self.tested = datetime.datetime.now(tz=datetime.timezone.utc)

    def classify(
        self, parameter: Model, clients: list[UnknownClient]
    ) -> list[UnknownClient]:
        """Классифицировать очередного клиента

        Args:
            parameter (Model): Модель дерева решений
            clients (Client): Клиент

        Returns:
            Clients: Классифицированные клиенты
        """
        classifications = parameter.classify(clients)
        for i, classification in enumerate(classifications):
            clients[i].classification = classification
        return clients


"""
Тестирование валидных/невалидных данных
"""

test_TrainingData = """
>>> td = TrainingData('test')
>>> filePath = Path('data.csv')
>>> reader = ClientReader(filePath)
>>> td.load(reader)
>>> mod = Model(max_depth = 2, min_samples_leaf = 1, training = td)
>>> len(td.training)
6
>>> len(td.testing)
2
>>> td.test(mod)
>>> print(f"data={td.name!r}, max_depth={mod.max_depth}, quality={mod.quality}")
data='test', max_depth=2, quality=0.5
"""

__test__ = {name: case for name, case in globals().items() if name.startswith("test_")}
