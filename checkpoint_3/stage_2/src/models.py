from __future__ import annotations

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import roc_auc_score

from dataclasses import dataclass
import datetime, csv, pathlib, abc
from typing import Any, Sequence, Set, cast, Iterator, NamedTuple, TypedDict, Callable

import weakref


class Domain(Set[int]):
    def validate(self, value: int) -> int | None:
        if value in self:
            return value
        raise ValueError(f"{value!r}")


class DomainKeys(Set[str]):
    def validate(self, keys: set[str]) -> None:
        if keys != self:
            raise ValueError(f"invalid fields")


class InvalidClientError(ValueError):
    """Исходные данные имеют недопустимое представление данных"""


class BadClientRow(ValueError):
    """Валидация плохой строки"""
    pass


class Client(NamedTuple):
    seniority: int
    home: int
    time: int
    age: int
    marital: int
    records: int
    job: int
    expenses: int
    income: int
    assets: int
    debt: int
    amount: int
    price: int

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}, "
            f"home={self.home}, "
            f"age={self.age}, "
            f"time={self.time}, "
            f"marital={self.marital}, "
            f"records={self.records}, "
            f"job={self.job}, "
            f"expenses={self.expenses}, "
            f"income={self.income}, "
            f"assets={self.assets}, "
            f"debt={self.debt}, "
            f"amount={self.amount}, "
            f"price={self.price}"
            f")"
        )

    @classmethod
    def from_dict(cls, row: ClientDict) -> Client:
        validKeys = DomainKeys(
            {
                "seniority",
                "home",
                "age",
                "time",
                "marital",
                "records",
                "job",
                "expenses",
                "income",
                "assets",
                "debt",
                "amount",
                "price",
                "status"
            }
        )
        try:
            validKeys.validate(set(row.keys()))
            return cls(
                seniority=int(row["seniority"]),
                home=int(row["home"]),
                time=int(row["time"]),
                age=int(row["age"]),
                marital=int(row["marital"]),
                records=int(row["records"]),
                job=int(row["job"]),
                expenses=int(row["expenses"]),
                income=int(row["income"]),
                assets=int(row["assets"]),
                debt=int(row["debt"]),
                amount=int(row["amount"]),
                price=int(row["price"]),
            )
        except ValueError:
            raise InvalidClientError(f"invalid keys in {row!r}")


class KnownClient(NamedTuple):
    client: Client
    status: int | None

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"client={self.client!r}, "
            f"status={self.status!r}"
            f")"
        )

    @classmethod
    def from_dict(cls, row: ClientDict) -> KnownClient:
        status = Domain([0, 1, 2])
        row_client = row.copy()
        try:
            return cls(
                Client.from_dict(row_client),
                status=status.validate(int(row["status"])),
            )
        except ValueError:
            raise InvalidClientError(f"invalid status in {row!r}")


class TrainingKnownClient(NamedTuple):
    client: KnownClient

    @classmethod
    def from_dict(cls, row: ClientDict) -> TrainingKnownClient:
        return cls(KnownClient.from_dict(row))

    def get_list_attributes(self) -> list[Any]:
        return list(self.client.client._asdict().values())


class TestingKnownClient:
    def __init__(self, client: KnownClient, classification: int | None = None) -> None:
        self.client = client
        self.classification = classification

    def matches(self) -> bool:
        return self.client.status == self.classification

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"client={self.client!r}, "
            f"classification={self.classification!r}"
            f")"
        )

    @classmethod
    def from_dict(cls, row: ClientDict) -> TestingKnownClient:
        return cls(KnownClient.from_dict(row))

    def get_list_attributes(self) -> list[Any]:
        return list(self.client.client._asdict().values())

class UnknownClient:
    def __init__(self, client: Client, classification: int | None = None) -> None:
        self.client = client
        self.classification = classification

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"client={self.client!r}, "
            f"classification={self.classification!r}"
            f")"
        )

    @classmethod
    def from_dict(cls, row: ClientDict) -> UnknownClient:
        return cls(Client.from_dict(row))

    def get_list_attributes(self) -> list[Any]:
        return list(self.client._asdict().values())


class ClientDict(TypedDict):
    seniority: int
    home: int
    time: int
    age: int
    marital: int
    records: int
    job: int
    expenses: int
    income: int
    assets: int
    debt: int
    amount: int
    price: int
    status: int


class ClientReader:
    """_summary_

    Returns:
        _type_: _description_
    """
    target_class = KnownClient
    header = [
        "seniority",
        "home",
        "age",
        "time",
        "marital",
        "records",
        "job",
        "expenses",
        "income",
        "assets",
        "debt",
        "amount",
        "price",
        "status",
    ]

    def __init__(self, source: pathlib.Path) -> None:
        self.source = source

    def client_iter_dict(self) -> Iterator[ClientDict]:
        with self.source.open() as source_file:
            reader = csv.DictReader(source_file, self.header)
            for row in reader:
                try:
                    client = ClientDict(
                        seniority=int(row["seniority"]),
                        home=int(row["home"]),
                        time=int(row["time"]),
                        age=int(row["age"]),
                        marital=int(row["marital"]),
                        records=int(row["records"]),
                        job=int(row["job"]),
                        expenses=int(row["expenses"]),
                        income=int(row["income"]),
                        assets=int(row["assets"]),
                        debt=int(row["debt"]),
                        amount=int(row["amount"]),
                        price=int(row["price"]),
                        status=int(row["status"])
                    )
                except ValueError as exception:
                    raise BadClientRow(f"invalid {row!r}")
                yield client

    def client_iter(self) -> Iterator[KnownClient]:
        target_class = self.target_class
        with self.source.open() as source_file:
            reader = csv.DictReader(source_file, self.header)
            for row in reader:
                try:
                    client = target_class.from_dict(ClientDict(
                        seniority=int(row["seniority"]),
                        home=int(row["home"]),
                        time=int(row["time"]),
                        age=int(row["age"]),
                        marital=int(row["marital"]),
                        records=int(row["records"]),
                        job=int(row["job"]),
                        expenses=int(row["expenses"]),
                        income=int(row["income"]),
                        assets=int(row["assets"]),
                        debt=int(row["debt"]),
                        amount=int(row["amount"]),
                        price=int(row["price"]),
                        status=int(row["status"])
                    ))
                except ValueError as exception:
                    raise BadClientRow(f"invalid {row!r}")
                yield client


class DealingPartition(abc.ABC):
    @abc.abstractmethod
    def __init__(
        self,
        items: list[ClientDict] | None,
        *,
        training_subset: tuple[int, int] = (8, 10),
    ) -> None:
        pass

    @abc.abstractmethod
    def extend(self, items: list[ClientDict]) -> None:
        pass

    @abc.abstractmethod
    def append(self, item: ClientDict) -> None:
        pass

    @property
    @abc.abstractmethod
    def training(self) -> list[TrainingKnownClient]:
        pass

    @property
    @abc.abstractmethod
    def testing(self) -> list[TestingKnownClient]:
        pass


class CountiongDealingPartition(DealingPartition):
    def __init__(
        self,
        items: list[ClientDict] | None,
        *,
        training_subset: tuple[int, int] = (8, 10),
    ) -> None:
        self.training_subset = training_subset
        self.counter = 0
        self._training: list[TrainingKnownClient] = []
        self._testing: list[TestingKnownClient] = []
        if items:
            self.extend(items)

    def extend(self, items: list[ClientDict]) -> None:
        for item in items:
            self.append(item)

    def append(self, item: ClientDict) -> None:
        n, d = self.training_subset
        if self.counter % d < n:
            self._training.append(TrainingKnownClient.from_dict(item))
        else:
            self._testing.append(TestingKnownClient.from_dict(item))
        self.counter += 1

    @property
    def training(self) -> list[TrainingKnownClient]:
        return self._training

    @property
    def testing(self) -> list[TestingKnownClient]:
        return self._testing


class Model:
    def __init__(
        self, max_depth: int, min_samples_leaf: int, training: "TrainingData"
    ) -> None:
        self.max_depth = max_depth
        self.min_samples_leaf = min_samples_leaf
        self.data: weakref.ReferenceType["TrainingData"] = weakref.ref(training)
        self.quality: float

    def test(self) -> None:
        """
        Проверка на тестовом наборе данных
        """
        training_data: "TrainingData" | None = self.data()
        if not training_data:
            raise RuntimeError("Broken Weak Reference")
        test_data = training_data.testing
        y_pred = self.classify(test_data)
        y_true = [test.client.status for test in test_data]
        self.quality = roc_auc_score(y_true, y_pred)
        for i in range(len(y_pred)):
            test_data[i].classification = y_pred[i]

    def classify(self, clients: Sequence[UnknownClient | TestingKnownClient]) -> Any:
        training_data = self.data()
        if not training_data:
            raise RuntimeError("No trainig object")
        x_train = [
            train_client.get_list_attributes()
            for train_client in training_data.training
        ]
        y_train = [train_client.client.status for train_client in training_data.training]
        x_pred = [client.get_list_attributes() for client in clients]

        classifier = DecisionTreeClassifier(
            max_depth=self.max_depth, min_samples_leaf=self.min_samples_leaf
        )
        classifier = classifier.fit(x_train, y_train)
        y_pred = classifier.predict(x_pred).tolist()
        return y_pred


class TrainingData:
    """Тренировочные данные"""

    def __init__(self, name: str) -> None:
        """

        Args:
            name (str): наименование набора
        """
        self.name = name
        self.uploaded: datetime.datetime
        self.tested: datetime.datetime
        self.tuning: list[Model] = []
        self.testing: list[TestingKnownClient] = []
        self.training: list[TrainingKnownClient] = []

    def load(self, partition: tuple[list[TrainingKnownClient], list[TestingKnownClient]]) -> None:
        self.training, self.testing = partition

    def test(self, parameter: Model) -> None:
        """Тестирование

        Args:
            parameter (Model): Модель
        """

        parameter.test()
        self.tuning.append(parameter)
        self.tested = datetime.datetime.now(tz=datetime.timezone.utc)

    def classify(
        self, parameter: Model, clients: list[UnknownClient]
    ) -> list[UnknownClient]:
        """Классифицировать очередного клиента

        Args:
            parameter (Model): Модель дерева решений
            clients (Client): Клиент

        Returns:
            Clients: Классифицированные клиенты
        """
        classifications = parameter.classify(clients)
        for i, classification in enumerate(classifications):
            clients[i].classification = classification
        return clients


"""
Тестирование валидных/невалидных данных
"""

test_Valid = """
>>> valid_client = {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 1}
>>> known_client = KnownClient.from_dict(valid_client)
>>> known_client
KnownClient(client=Client(seniority=17, home=1, age=58, time=60, marital=3, records=1, job=1, expenses=48, income=131, assets=0, debt=0, amount=1000, price=1685), status=1)

>>> training_known_client = TrainingKnownClient.from_dict(valid_client)
>>> training_known_client
TrainingKnownClient(client=KnownClient(client=Client(seniority=17, home=1, age=58, time=60, marital=3, records=1, job=1, expenses=48, income=131, assets=0, debt=0, amount=1000, price=1685), status=1))

>>> valid_uc = valid_client.copy()
>>> uc = UnknownClient.from_dict(valid_uc)
>>> uc
UnknownClient(client=Client(seniority=17, home=1, age=58, time=60, marital=3, records=1, job=1, expenses=48, income=131, assets=0, debt=0, amount=1000, price=1685), classification=None)
"""

test_Invalid = """
>>> invalid_status = {"seniority": 17, "home": 1, "age": 58, "time": 60, "marital": 3, "records": 1, "job": 1, "expenses": 48, "income": 131, "assets": 0, "debt": 0, "amount": 1000, "price": 1685, "status": 3}
>>> known_client = KnownClient.from_dict(invalid_status)
Traceback (most recent call last):
...
models.InvalidClientError: invalid status in {'seniority': 17, 'home': 1, 'age': 58, 'time': 60, 'marital': 3, 'records': 1, 'job': 1, 'expenses': 48, 'income': 131, 'assets': 0, 'debt': 0, 'amount': 1000, 'price': 1685, 'status': 3}

>>> invalid_uc = invalid_status.copy()
>>> del invalid_uc['status']
>>> invalid_uc['j'] = invalid_uc['job']
>>> del invalid_uc['job']
>>> uc = UnknownClient.from_dict(invalid_uc)
Traceback (most recent call last):
...
models.InvalidClientError: invalid keys in {'seniority': 17, 'home': 1, 'age': 58, 'time': 60, 'marital': 3, 'records': 1, 'expenses': 48, 'income': 131, 'assets': 0, 'debt': 0, 'amount': 1000, 'price': 1685, 'j': 1}
"""

"""
Тестирование загрузки клиентов и работы обучения нейронной сети
"""
test_Client = """
>>> c_1 = Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846)
>>> c_1
Client(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846)
"""

test_TrainingKnownClient = """
>>> c_4 = TrainingKnownClient(KnownClient(Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846), status = 1))
>>> c_4
TrainingKnownClient(client=KnownClient(client=Client(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846), status=1))
"""

test_TestingKnownSample = """
>>> c_3 = TestingKnownClient(client=KnownClient(Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846), status = 1))
>>> c_3.classification = 1
>>> c_3
TestingKnownClient(client=KnownClient(client=Client(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846), status=1), classification=1)
"""

test_UnknownClient = """
>>> c_2 = UnknownClient(Client(seniority = 9, home = 1, age = 30, time = 60, marital = 2, records = 1, job = 4, expenses = 73, income = 129, assets = 0, debt = 0, amount = 800, price = 846))
>>> c_2
UnknownClient(client=Client(seniority=9, home=1, age=30, time=60, marital=2, records=1, job=4, expenses=73, income=129, assets=0, debt=0, amount=800, price=846), classification=None)
"""

test_TrainingData = """
>>> td = TrainingData('test')
>>> filePath = pathlib.Path('data.csv')
>>> reader = ClientReader(filePath)
>>> data = CountiongDealingPartition([item for item in reader.client_iter_dict()])
>>> td = TrainingData('test')
>>> td.load((data.training, data.testing))
>>> mod = Model(max_depth = 2, min_samples_leaf = 1, training = td)
>>> len(td.training)
10
>>> len(td.testing)
2
>>> td.test(mod)
>>> print(f"data={td.name!r}, max_depth={mod.max_depth}, quality={mod.quality}")
data='test', max_depth=2, quality=1.0
"""

__test__ = {name: case for name, case in globals().items() if name.startswith("test_")}
