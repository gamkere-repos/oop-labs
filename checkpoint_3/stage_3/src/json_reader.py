from pathlib import Path
from src.models import ClientDict
from typing import Iterator, Any
import json, jsonschema

CLIENT_SCHEMA = {
    "$schema": "https://json-schema.org/draft/2019-09/hyper-schema",
    "title": "Client Data Schema",
    "descriptiom": "Schema of Credit Scoring",
    "type": "object",
    "properties": {
        "seniority": {
            "type": "number",
        },
        "home": {
            "type": "number",
        },
        "age": {
            "type": "number",
        },
        "time": {
            "type": "number",
        },
        "marital": {
            "type": "number",
        },
        "records": {
            "type": "number",
        },
        "job": {
            "type": "number",
        },
        "expenses": {
            "type": "number",
        },
        "income": {
            "type": "number",
        },
        "assets": {
            "type": "number",
        },
        "debt": {
            "type": "number",
        },
        "amount": {
            "type": "number",
        },
        "price": {
            "type": "number",
        },
        "status": {
            "type": "number", "enum": [0, 1, 2]
        }
    },
    "required": [
        "seniority", "home", "age", "time", "marital", "records", "job", "expenses", "income", "assets", "debt", "amount", "price", "status"
    ]
}


class JSONClientReader:
    def __init__(self, source: Path) -> None:
        self.source = source

    def client_iter_dict(self) -> Iterator[ClientDict]:
        with self.source.open() as source_file:
            for line in source_file:
                client =  json.loads(line)
                yield client

class ValidatedNDJSONClientReader:
    def __init__(self, source: Path, schema: dict[str, Any]) -> None:
        self.source = source
        self.validator = jsonschema.Draft7Validator(schema)

    def client_iter_dict(self) -> Iterator[ClientDict]:
        with self.source.open() as source_file:
            for line in source_file:
                client = json.loads(line)
                if self.validator.is_valid(client):
                    yield client
                else:
                    print(f"ivalid: {client}")


test_JSON_serialize = """
>>> from src.models import *
>>> p = pathlib.Path("data.csv")
>>> reader = ClientReader(p)
>>> for i in reader.client_iter_dict():
...     print(i)
{'seniority': 9, 'home': 1, 'time': 30, 'age': 60, 'marital': 2, 'records': 1, 'job': 3, 'expenses': 73, 'income': 129, 'assets': 0, 'debt': 0, 'amount': 800, 'price': 846, 'status': 1}
{'seniority': 17, 'home': 1, 'time': 58, 'age': 60, 'marital': 3, 'records': 1, 'job': 1, 'expenses': 48, 'income': 131, 'assets': 0, 'debt': 0, 'amount': 1000, 'price': 1658, 'status': 1}
{'seniority': 10, 'home': 2, 'time': 46, 'age': 36, 'marital': 2, 'records': 2, 'job': 3, 'expenses': 90, 'income': 200, 'assets': 3000, 'debt': 0, 'amount': 2000, 'price': 2985, 'status': 2}
{'seniority': 0, 'home': 4, 'time': 36, 'age': 48, 'marital': 2, 'records': 1, 'job': 2, 'expenses': 45, 'income': 130, 'assets': 750, 'debt': 0, 'amount': 1100, 'price': 1511, 'status': 2}
{'seniority': 2, 'home': 1, 'time': 25, 'age': 60, 'marital': 1, 'records': 1, 'job': 1, 'expenses': 46, 'income': 107, 'assets': 0, 'debt': 0, 'amount': 1500, 'price': 2189, 'status': 2}
{'seniority': 5, 'home': 2, 'time': 22, 'age': 60, 'marital': 1, 'records': 1, 'job': 1, 'expenses': 45, 'income': 324, 'assets': 10000, 'debt': 0, 'amount': 1100, 'price': 1159, 'status': 1}
{'seniority': 1, 'home': 2, 'time': 45, 'age': 60, 'marital': 2, 'records': 1, 'job': 2, 'expenses': 105, 'income': 112, 'assets': 2000, 'debt': 500, 'amount': 600, 'price': 1332, 'status': 2}
{'seniority': 27, 'home': 1, 'time': 41, 'age': 60, 'marital': 2, 'records': 1, 'job': 1, 'expenses': 74, 'income': 140, 'assets': 0, 'debt': 0, 'amount': 950, 'price': 1497, 'status': 1}
{'seniority': 2, 'home': 1, 'time': 25, 'age': 60, 'marital': 1, 'records': 1, 'job': 1, 'expenses': 46, 'income': 107, 'assets': 0, 'debt': 0, 'amount': 1500, 'price': 2189, 'status': 2}
{'seniority': 5, 'home': 2, 'time': 22, 'age': 60, 'marital': 1, 'records': 1, 'job': 1, 'expenses': 45, 'income': 324, 'assets': 10000, 'debt': 0, 'amount': 1100, 'price': 1159, 'status': 1}
{'seniority': 1, 'home': 2, 'time': 45, 'age': 60, 'marital': 2, 'records': 1, 'job': 2, 'expenses': 105, 'income': 112, 'assets': 2000, 'debt': 500, 'amount': 600, 'price': 1332, 'status': 2}
{'seniority': 27, 'home': 1, 'time': 41, 'age': 60, 'marital': 2, 'records': 1, 'job': 1, 'expenses': 74, 'income': 140, 'assets': 0, 'debt': 0, 'amount': 950, 'price': 1497, 'status': 1}
"""

__test__ = {name: case for name, case in globals().items() if name.startswith("test_")}