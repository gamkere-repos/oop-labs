from collections import defaultdict

from typing import Callable
from src.models import TestingKnownClient, TrainingKnownClient, KnownClient

def training_80(client: KnownClient, i: int) -> bool:
    return i % 5 != 0

def training_75(client: KnownClient, i: int) -> bool:
    return i % 4 != 0

def training_67(client: KnownClient, i: int) -> bool:
    return i % 3 != 0

def partition(
    clients: list[KnownClient],
    rule: Callable[[KnownClient, int], bool]
) -> tuple[list[TrainingKnownClient], list[TestingKnownClient]]:
    pools: defaultdict[bool, list[KnownClient]] = defaultdict(list)
    partition = ((rule(client, i), client) for i, client in enumerate(clients))

    for usage_pool, client in partition:
        pools[usage_pool].append(client)

    training = [TrainingKnownClient(client) for client in pools[True]]
    testing = [TestingKnownClient(client) for client in pools[False]]
    return training, testing

test_Partition = """
>>> from src.models import *
>>> p = pathlib.Path("data.csv")
>>> reader = ClientReader(p)
>>> training, testing = partition([known_client for known_client in reader.client_iter()], training_80)
>>> len(training)
9
>>> len(testing)
3
"""

__test__ = {name: case for name, case in globals().items() if name.startswith("test_")}