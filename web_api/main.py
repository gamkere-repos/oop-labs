from web_api.api import API

app = API()

@app.route("/client")
class ClientResource:
    def get(self, req, resp):
        resp.text = "Client Page"

    def post(self, req, resp):
        resp.text = "Create Client"
